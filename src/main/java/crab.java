/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author User
 */
public class crab extends AquaticAnimal {

    private String name;

    public crab(String name) {
        super("crab", 10);
        this.name = name;

    }

    @Override
    public void swim() {
        System.out.println("Crab: " + name + " swim");
    }

    @Override
    public void eat() {
        System.out.println("Crab: " + name + " eat");
    }

    @Override
    public void walk() {
        System.out.println("Crab: " + name + " walk");
    }

    @Override
    public void speak() {
        System.out.println("Crab: " + name + " not speak");
    }

    @Override
    public void sleep() {
        System.out.println("Crab: " + name + " sleep");
    }
}
