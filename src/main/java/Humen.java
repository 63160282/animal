/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author User
 */
public class Humen extends LandAnimal {

    private String nickname;

    public Humen(String nickname) {
        super("Human", 2);
        this.nickname = nickname;
    }

    @Override
    public void run() {
        System.out.println("Humen: "+nickname+" run");
    }

    @Override
    public void eat() {
        System.out.println("Humen: "+nickname+" eat");
    }

    @Override
    public void walk() {
        System.out.println("Humen: "+nickname+" walk");
    }

    @Override
    public void speak() {
        System.out.println("Humen: "+nickname+" sleak");
    }

    @Override
    public void sleep() {
        System.out.println("Humen: "+nickname+" sleep");
    }

}
