/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author User
 */
public class Cat extends LandAnimal{
    private String name;

    public Cat(String name) {
        super("cat", 4);
        this.name = name;
    }

    @Override
    public void run() {
        System.out.println("cat: "+name+" run");
    }

    @Override
    public void eat() {
        System.out.println("cat: "+name+" eat");
    }

    @Override
    public void walk() {
        System.out.println("cat: "+name+" walk");
    }

    @Override
    public void speak() {
        System.out.println("cat: "+name+" speak");
    }

    @Override
    public void sleep() {
        System.out.println("cat: "+name+" sleep");
    }
    
    
}
